#mvn liquibase:generateChangeLog

#@echo off
#
#for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
#set ldt=%ldt:~0,4%%ldt:~4,2%%ldt:~6,2%_%ldt:~8,2%%ldt:~10,2%%ldt:~12,2%
#
#mvnw-clean-install.bat && mvnw liquibase:diff -Plocal -Dliquibase.propertyFile=src/main/resources/db/liquibase.properties -Dliquibase.diffChangeLogFile=src/main/resources/changelog_%ldt%.xml

# mvn liquibase:updateSQL # check sql before updating
# mvn liquibase:update