package com.example.demo.validators;

import com.example.demo.models.forms.users.UserInsertForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, UserInsertForm> {
    @Override
    public void initialize(PasswordMatch p) {

    }

    public boolean isValid(UserInsertForm user, ConstraintValidatorContext c) {
        String plainPassword = user.getPlainPassword();
        String repeatPassword = user.getPassword();

        if(plainPassword == null || !plainPassword.equals(repeatPassword)) {
            return false;
        }

        return true;
    }
}
