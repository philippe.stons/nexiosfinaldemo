package com.example.demo.listeners;

import com.example.demo.enums.MessageType;
import com.example.demo.models.ChatMessage;
import com.example.demo.services.impl.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
@Slf4j
public class WebSocketListener {
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private RoomService roomService;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event)
    {
        log.info("Received new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event)
    {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        String roomName = (String) headerAccessor.getSessionAttributes().get("roomName");

        if(username != null)
        {
            log.info("Disconect : " + username);

            ChatMessage msg = new ChatMessage();
            msg.setType(MessageType.LEAVE);
            msg.setSender(username);
            roomService.removeUserFromRoom(roomName);

            messagingTemplate.convertAndSend("/topic/" + roomName, msg);
        }
    }
}
