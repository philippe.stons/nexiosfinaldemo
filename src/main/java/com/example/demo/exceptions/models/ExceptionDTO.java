package com.example.demo.exceptions.models;

import lombok.*;

import java.time.Instant;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ExceptionDTO {

    private String message;
    private Instant timestamp = Instant.now();

    public ExceptionDTO(String message){ this.message = message; }

}
