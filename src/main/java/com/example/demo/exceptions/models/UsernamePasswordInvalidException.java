package com.example.demo.exceptions.models;

public class UsernamePasswordInvalidException extends RuntimeException {

    public UsernamePasswordInvalidException(){ super("Username/password invalid"); }

}
