package com.example.demo.controllers;

import com.example.demo.models.dtos.RoomDTO;
import com.example.demo.models.forms.rooms.RoomInsertForm;
import com.example.demo.services.impl.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/rooms")
public class RoomController {
    private final RoomService roomService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<List<RoomDTO>> getAll()
    {
        return ResponseEntity.ok(roomService.getAll());
    }

    @GetMapping("/{id}")
    @PreAuthorize("authentication.principal != null")
    public RoomDTO getOneRoom(HttpServletRequest request, @PathVariable(name = "id") Long id)
    {
        return roomService.getOneById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("authentication.principal != null")
    public RoomDTO connectToRoom(HttpServletRequest request, @PathVariable(name = "id") Long id)
    {
        RoomDTO room = roomService.getOneById(id);
        roomService.addUserFromRoom(id);

        return room;
    }

    @PutMapping("/{id}/leave")
    @PreAuthorize("authentication.principal != null")
    public RoomDTO leaveRoom(HttpServletRequest request, @PathVariable(name = "id") Long id)
    {
        log.info("LEAVE ROOM");

        return roomService.getOneById(id);
    }


    @PostMapping("add")
    public ResponseEntity<RoomDTO> addRoom(@Validated RoomInsertForm form)
    {
        return ResponseEntity.ok(roomService.insert(form));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Long> deleteRoom(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(roomService.delete(id));
    }
}
