package com.example.demo.controllers;

import com.example.demo.models.dtos.UserDTO;
import com.example.demo.models.forms.users.UserInsertForm;
import com.example.demo.services.FooService;
import com.example.demo.services.impl.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    @Qualifier(value = "fooService1")
    private final FooService fooService;

    public UserController(UserService userService, @Qualifier(value = "fooService1") FooService fooService) {
        this.userService = userService;
        this.fooService = fooService;
    }

    @GetMapping()
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<List<UserDTO>> getAllUsers(){
        return ResponseEntity.ok(this.userService.getAll());
    }

    @PostMapping("")
    @PreAuthorize("!hasAnyAuthority('USER') or hasAnyAuthority('ADMIN')")
    public ResponseEntity<UserDTO> postAddUser(@Valid @RequestBody UserInsertForm userForm)
    {
        return ResponseEntity.ok(this.userService.insert(userForm));
    }

    @PutMapping("{id}")
    @PreAuthorize("authentication.principal.getId() == #id or hasAnyAuthority('ADMIN')")
    public ResponseEntity<UserDTO> postAddPerson(@Valid @RequestBody UserInsertForm userFor, @PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.userService.update(userFor, id));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("authentication.principal.getId() == #id or hasAnyAuthority('ADMIN')")
    public ResponseEntity<Long> deletePerson(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.userService.delete(id));
    }
}
