package com.example.demo.controllers;

import com.example.demo.models.ChatMessage;
import com.example.demo.services.impl.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class WebSocketController {
    private final RoomService roomService;

    @MessageMapping("/chat.sendMessage/{roomName}")
    @SendTo("/topic/{roomName}")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage, @DestinationVariable String roomName)
    {
        log.info("MESSAGE SEND");
        log.info(chatMessage.getContent());
        log.info(roomName);

        return chatMessage;
    }

    @MessageMapping("/chat.addUser/{roomName}")
    @SendTo("/topic/{roomName}")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor, @DestinationVariable String roomName)
    {
        log.info(roomName);
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        headerAccessor.getSessionAttributes().put("roomName", roomName);
//        headerAccessor.getUser()

        return chatMessage;
    }
}
