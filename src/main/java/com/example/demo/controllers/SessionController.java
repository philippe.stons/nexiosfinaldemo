package com.example.demo.controllers;

import com.example.demo.models.dtos.UserDTO;
import com.example.demo.models.forms.users.UserInsertForm;
import com.example.demo.models.forms.users.UserLoginForm;
import com.example.demo.services.impl.SessionService;
import com.example.demo.services.impl.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class SessionController {
    private final SessionService service;
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody UserLoginForm form)
    {
        return ResponseEntity.ok(service.login(form));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> register(@Valid @RequestBody UserInsertForm form)
    {
        return ResponseEntity.ok(userService.insert(form));
    }
}
