package com.example.demo.mappers;

import com.example.demo.models.dtos.RoomDTO;
import com.example.demo.models.entities.Room;
import com.example.demo.models.forms.rooms.RoomInsertForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface RoomMapper {
    @Mapping(target = "id", source = "roomId")
    RoomDTO entityToDto(Room room);

    void formToEntity(RoomInsertForm form, @MappingTarget Room room);
}
