package com.example.demo.mappers;

import com.example.demo.mappers.mappings.EncodedMapping;
import com.example.demo.models.dtos.UserDTO;
import com.example.demo.models.entities.User;
import com.example.demo.models.forms.users.UserInsertForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = {PasswordEncodeMapper.class}, componentModel = "spring")
public interface UserMapper {
    UserDTO entityToDto(User user);

    @Mapping(source = "password", target = "password", qualifiedBy = EncodedMapping.class)
    void formToEntity(UserInsertForm form, @MappingTarget User user);
}
