package com.example.demo.mappers;

import com.example.demo.mappers.mappings.EncodedMapping;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PasswordEncodeMapper {
    final PasswordEncoder encoder;

    @EncodedMapping
    public String encode(String value)
    {
        return encoder.encode(value);
    }
}
