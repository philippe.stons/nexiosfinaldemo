package com.example.demo.enums;

public enum MessageType
{
    MESSAGE,
    JOIN,
    LEAVE
}
