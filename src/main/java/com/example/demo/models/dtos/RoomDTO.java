package com.example.demo.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoomDTO {
    private Long id;
    private String name;
}
