package com.example.demo.models.forms.users;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginForm {
    private String username;

    private String password;
}
