package com.example.demo.models.forms.rooms;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@AllArgsConstructor
@NoArgsConstructor
public class RoomInsertForm {
    @Length(min = 4, max = 100)
    private String name;
}

