package com.example.demo.models.forms.users;

import com.example.demo.validators.PasswordMatch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;

@Data
@Validated
@NoArgsConstructor
@AllArgsConstructor
@PasswordMatch
public class UserInsertForm {
    @Length(min = 4, max = 20)
    private String username;

    @Email()
    private String email;

    @Length(min = 5)
    private String password;

    @Length(min = 5)
    private String plainPassword;
}
