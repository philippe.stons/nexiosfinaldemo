package com.example.demo.models.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "rooms")
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long roomId;

    @Column(nullable = false, unique = true, length = 100, name = "roomname")
    String name;

    @Column(nullable = false, columnDefinition = "integer default 0")
    Integer connectedUsers;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "RoomUsers",
            joinColumns = { @JoinColumn(name = "roomId") },
            inverseJoinColumns = { @JoinColumn(name = "id")}
    )
    List<User> users;
}

