package com.example.demo.models.entities.abc;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;

@MappedSuperclass
public abstract class BaseEntity {
    @Column(name = "isActive", nullable = false)
    protected boolean isActive = true;

    @Column(name = "createdAt", nullable = false)
    protected LocalDate createdAt;

    @Column(name = "updatedAt")
    protected LocalDate updatedAt;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public abstract void prePersist();
    public abstract void preUpdate();
}
