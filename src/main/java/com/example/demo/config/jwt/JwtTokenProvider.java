package com.example.demo.config.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.config.SecurityConst;
import com.example.demo.models.entities.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtTokenProvider {
    private final UserDetailsService service;

    public String createToken(User user)
    {
        return SecurityConst.TOKEN_PREFIX + JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConst.EXPIRATION_TIME))
                .withClaim("roles", user.getRoles())
                .sign(Algorithm.HMAC512(SecurityConst.JWT_KEY));
    }

    public String resolveToken(HttpServletRequest request)
    {
        String token = request.getHeader(SecurityConst.HEADER_KEY);

        if(token != null && token.startsWith(SecurityConst.TOKEN_PREFIX))
        {
            return token.substring(SecurityConst.TOKEN_PREFIX.length());
        }

        return null;
    }

    public boolean validateToken(String token)
    {
        try
        {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC512(SecurityConst.JWT_KEY))
                    .build().verify(token.replace(SecurityConst.TOKEN_PREFIX, ""));

            String username = decodedJWT.getSubject();
            Date exp = decodedJWT.getExpiresAt();

            return username != null && exp != null && exp.after(new Date(System.currentTimeMillis()));
        }
        catch (JWTVerificationException e)
        {
            log.error(e.getMessage());

            return false;
        }
    }

    public Authentication getAuthentication(String token)
    {
        String userName = JWT.decode(token.replace(SecurityConst.TOKEN_PREFIX, "")).getSubject();
        UserDetails userDetails = service.loadUserByUsername(userName);
        return new UsernamePasswordAuthenticationToken(userDetails,  null, userDetails.getAuthorities());
    }
}
