package com.example.demo.config;

import com.example.demo.config.jwt.JwtTokenProvider;
import com.example.demo.services.impl.HttpHandshakeInterceptor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.core.Authentication;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;

@Configuration
@Slf4j
@EnableWebSocketMessageBroker
@AllArgsConstructor
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    private final JwtTokenProvider tokenProvider;

    @Autowired
    private HttpHandshakeInterceptor interceptor;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
                .setHandshakeHandler(defaultHandshakeHandler())
                .setAllowedOriginPatterns("*")
                .withSockJS().setInterceptors(interceptor);
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app");
        registry.enableSimpleBroker("/topic");
    }

    private DefaultHandshakeHandler defaultHandshakeHandler() {
        return new DefaultHandshakeHandler() {
            @Override
            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {

                int startIndex;
                int endIndex;

                try {
                    startIndex = request.getURI().getQuery().indexOf("access_token=") + "access_token=".length();
                    endIndex = request.getURI().getQuery().indexOf('&');
                } catch (NullPointerException e) {
                    log.debug("Could not determine user from the request : " + request.getURI());
                    return null;
                }

                String token;

                if (endIndex == -1) {
                    token = request.getURI().getQuery().substring(startIndex);
                } else {
                    token = request.getURI().getQuery().substring(startIndex, endIndex);
                }

                if (token.trim().length() == 0) {
                    log.debug("No JWT found in the request to " + request.getURI());
                    return null;
                }

                Authentication authentication = tokenProvider.getAuthentication(token);

                if (authentication == null || authentication.getPrincipal() == null) {
                    log.debug("No principal found in the request to " + request.getURI());
                    return null;
                } else {
                    Principal principal = (Principal) authentication.getPrincipal();
                    log.debug("Websocket connection opened for " + principal.getName());
                    return principal;
                }
            }
        };
    }
}
