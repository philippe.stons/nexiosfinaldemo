package com.example.demo.repositories;

import com.example.demo.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findUserByUsername(String username);

    @Query(value = "SELECT u FROM User u WHERE u.enabled = true")
    public List<User> findEnabled();

    @Query(value = "SELECT u FROM User u WHERE u.email = :email")
    public User findByEmail(String email);
}
