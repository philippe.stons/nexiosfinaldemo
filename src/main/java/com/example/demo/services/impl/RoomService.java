package com.example.demo.services.impl;

import com.example.demo.mappers.RoomMapper;
import com.example.demo.models.dtos.RoomDTO;
import com.example.demo.models.entities.Room;
import com.example.demo.models.entities.User;
import com.example.demo.models.forms.rooms.RoomInsertForm;
import com.example.demo.repositories.RoomRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.BaseService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class RoomService implements BaseService<RoomDTO, RoomInsertForm, Long> {
    private final RoomRepository repository;
    private final RoomMapper mapper;
    private final UserRepository userRepository;

    @Override
    public List<RoomDTO> getAll() {
        return repository.findAll().stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public RoomDTO getOneById(Long id) {
        return repository.findById(id)
                .map(mapper::entityToDto)
                .orElse(null);
    }

    public boolean checkAuthorization(String roomName)
    {
        Room room = repository.findRoomByName(roomName).orElse(null);
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(room == null || (room != null && room.getUsers() == null))
        {
            return false;
        }

        for (User user : room.getUsers()) {
            if(user.getId().equals(principal.getId()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public RoomDTO insert(RoomInsertForm form) {
        Room room = new Room();
        room.setConnectedUsers(0);

        mapper.formToEntity(form, room);
        repository.save(room);

        return mapper.entityToDto(room);
    }

    @Override
    public Long delete(Long id) {
        Room room = repository.findById(id).orElse(null);

        repository.delete(room);

        return room.getRoomId();
    }

    @Override
    public RoomDTO update(RoomInsertForm form, Long id) {
        Room room = repository.findById(id).orElse(null);

        mapper.formToEntity(form, room);

        return mapper.entityToDto(room);
    }

    public RoomDTO addUserFromRoom(Long id)
    {
        Room room = repository.findById(id).orElse(null);
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = this.userRepository.findById(principal.getId()).orElse(null);

        if(room != null)
        {
            room.setConnectedUsers(room.getConnectedUsers() + 1);
            room.getUsers().add(user);
            log.info("ADD USER : " + room.getConnectedUsers().toString());
        }
        repository.flush();

        return mapper.entityToDto(room);
    }

    public RoomDTO removeUserFromRoom(Long id)
    {
        Room room = repository.findById(id).orElse(null);
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = this.userRepository.findById(principal.getId()).orElse(null);

        if(room != null)
        {
            room.setConnectedUsers(room.getConnectedUsers() <= 0 ? 0 : room.getConnectedUsers() - 1);
            room.getUsers().remove(user);
            log.info("REMOVE USER : " + room.getConnectedUsers().toString());
        }
        repository.flush();

        return mapper.entityToDto(room);
    }

    public RoomDTO removeUserFromRoom(String name)
    {
        Room room = repository.findRoomByName(name).orElse(null);

        if(room != null)
        {
            room.setConnectedUsers(room.getConnectedUsers() <= 0 ? 0 : room.getConnectedUsers() - 1);
            log.info("REMOVE USER : " + room.getConnectedUsers().toString());
        }
        repository.flush();

        return mapper.entityToDto(room);
    }
}
