package com.example.demo.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl {
    private final JavaMailSender sender;

    public void sendSimpleMessage(String to, String subject, String text)
    {
        SimpleMailMessage msg = new SimpleMailMessage();

        msg.setFrom("ps.dev.noreply@gmail.com");
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setText(text);
        sender.send(msg);
    }
}
