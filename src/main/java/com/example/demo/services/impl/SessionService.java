package com.example.demo.services.impl;

import com.example.demo.config.jwt.JwtTokenProvider;
import com.example.demo.mappers.UserMapper;
import com.example.demo.models.dtos.UserDTO;
import com.example.demo.models.entities.User;
import com.example.demo.models.forms.users.UserLoginForm;
import com.example.demo.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SessionService {
    final UserRepository userRepository;
    final AuthenticationManager manager;
    final UserMapper userMapper;
    final JwtTokenProvider provider;

    public UserDTO login(UserLoginForm form)
    {
        User u = userRepository.findUserByUsername(form.getUsername());

        if(u != null)
        {
            this.manager.authenticate(new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword()));

            UserDTO user = userMapper.entityToDto(u);
            user.setToken(provider.createToken(u));

            return user;
        }
        return null;
    }
}
