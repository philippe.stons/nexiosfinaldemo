package com.example.demo.services.impl;

import com.example.demo.mappers.UserMapper;
import com.example.demo.models.dtos.UserDTO;
import com.example.demo.models.entities.User;
import com.example.demo.models.forms.users.UserInsertForm;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService implements BaseService<UserDTO, UserInsertForm, Long>, UserDetailsService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final EmailServiceImpl email;

    @Override
    public List<UserDTO> getAll()
    {
        return this.userRepository.findAll()
                .stream()
                .map(userMapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getOneById(Long id) {
        return this.userRepository.findById(id)
                .map(userMapper::entityToDto)
                .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));
    }

    public UserDTO getOneByUsername(String username) {
        return userMapper.entityToDto(this.userRepository.findUserByUsername(username));
    }

    public User findOneByUsername(String username) {
        return this.userRepository.findUserByUsername(username);
    }

    @Override
    public UserDTO insert(UserInsertForm form) {

        User user = new User();
        userMapper.formToEntity(form, user);

        if(user.getRoles() == null)
        {
            user.setRoles(new ArrayList<String>());
        }

        user.getRoles().add("USER");

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        email.sendSimpleMessage(user.getEmail(), "Welcome", "Hello world!");

        return userMapper.entityToDto(userRepository.save(user));
    }

    @Override
    public Long delete(Long id) {
        User user = this.userRepository.findById(id).orElse(null);

        user.setActive(false);
        this.userRepository.save(user);
//        this.userRepository.delete(user);

        return user.getId();
    }

    @Override
    public UserDTO update(UserInsertForm form, Long id) {
        User user = this.userRepository.findById(id).orElse(null);

        userMapper.formToEntity(form, user);

        return userMapper.entityToDto(userRepository.save(user));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username);
    }
}
