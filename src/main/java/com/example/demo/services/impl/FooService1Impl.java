package com.example.demo.services.impl;

import com.example.demo.services.FooService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Qualifier(value = "fooService1")
public class FooService1Impl implements FooService {
    @Override
    public void SuperFunction() {
        log.info("CALLED IN FOO SERVICE 1");
    }
}